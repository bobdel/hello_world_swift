//
//  ViewController.swift
//  Hello World Swift
//
//  Created by Robert DeLaurentis on 11/25/14.
//  Copyright (c) 2014 Robert DeLaurentis. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var mainTextField: UITextField!

    @IBAction func mainButton(sender: AnyObject) {
        if self.mainTextField.text.isEmpty {
            self.mainLabel.text = "Hello, World!"
        } else {
            self.mainLabel.text = "Hello, " + self.mainTextField.text + "!"
        }

    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            mainButton(self)
            return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }




}

